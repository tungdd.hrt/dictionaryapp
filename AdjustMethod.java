import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdjustMethod extends JFrame implements ActionListener
{
    private JTextField textField1;
    private JButton searchButton;
    private JTextArea textArea1;
    private JButton changeButton;
    private JLabel Activity;
    private JPanel ChangePanel;

    public AdjustMethod()
    {
        initComponents();
        initActionListener();
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource().equals(searchButton))
        {
            setSearchButton();
        }
        if (e.getSource().equals(changeButton))
        {
            setChangeButton();
        }
    }

    public void initComponents()
    {
        setTitle("Test App");
        setSize(600, 400);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setContentPane(ChangePanel);
        setVisible(false);
    }

    public void initActionListener()
    {
        searchButton.addActionListener(this);
        changeButton.addActionListener(this);
    }

    public void setSearchButton()
    {
        String s = textField1.getText();
        String res = DictionaryAppMethod.dictionaryLookup(s);
        textArea1.setText(res);
    }

    public void setChangeButton()
    {
        String word_target = textField1.getText();
        String word_explain = textArea1.getText();
        String status = DictionaryAppMethod.editWord(word_target, word_explain);
        Activity.setText(status);
    }
}
