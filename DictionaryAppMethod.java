import java.io.*;
import java.util.Optional;
import java.util.Scanner;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;


public class DictionaryAppMethod
{

    Dictionary myDictionary = new Dictionary();

    public static boolean empty(final String s)
    {
        return s == null || s.trim().isEmpty();
    }

    public static void insertFromFile()
    {
        try
        {
            File text = new File("dictionary.txt");
            Scanner file = new Scanner(text);
            String a = file.nextLine();
            String target;
            while (file.hasNextLine())
            {
                if (!empty(a))
                {
                    target = a;
                } else
                {
                    target = file.nextLine();
                }
                if (empty(target)) continue;
                String explain = "";
                Word newWord = new Word();

                if (target.charAt(0) == '@')
                {
                    target = target.substring(1);
                    newWord.setWord_target(target);
                    a = file.nextLine();
                    if (empty(a)) continue;

                    while (a.charAt(0) != '@')
                    {
                        explain += a + '\n';
                        if (!file.hasNextLine()) break;
                        a = file.nextLine();
                        if (empty(a)) break;
                    }
                    newWord.setWord_explain(explain);
                }
                if (!empty(newWord.getWord_explain()) && !empty(newWord.getWord_target()))
                {
                    Dictionary.listWord.add(newWord);
                }
            }
        } catch (FileNotFoundException f)
        {
            f.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * find the word in the dictionary and show it's mean
     * return void
     */
    public static String dictionaryLookup(String a)
    {
        String res = "";
        for (Word i : Dictionary.listWord)
        {
            if (a.equalsIgnoreCase(i.getWord_target()))
            {
                res += i.getWord_explain();
                return res;
            }
        }
        return "The target word does not exist!";
    }

    /**
     * The method puts all the word
     * of current dictionary in File "DictionaryNow.txt"
     */
    public static String dictionaryExportToFile()
    {
        try
        {
            File fout = new File("DictionaryNow.txt");
            FileWriter fw = new FileWriter(fout);
            BufferedWriter br = new BufferedWriter(fw);

            for (int i = 0; i < Dictionary.listWord.size(); i++)
            {
                br.write('@' + Dictionary.listWord.get(i).getWord_target() + '\n');
                br.write(Dictionary.listWord.get(i).getWord_explain() + '\n');
            }
            br.close();
            fw.close();
            return "Saved success!";
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        return "Saved error!";
    }

    public static String insertWord(String Word_target, String Word_explain)
    {
        for (Word i : Dictionary.listWord)
        {
            if (i.getWord_target().equalsIgnoreCase(Word_target))
            {
                return "This word has already existed!";
            }
        }
        Dictionary.listWord.add(new Word(Word_target, Word_explain));
        return "Insert Complete!";
    }

    public static String removeWord(String word_target)
    {
        boolean check = false;
        for (int i = 0; i < Dictionary.listWord.size(); i++)
        {
            if (Dictionary.listWord.get(i).getWord_target().equalsIgnoreCase(word_target))
            {
                Dictionary.listWord.remove(i);
                check = true;
            }
        }
        if (check)
        {
            return "Removed!";
        } else
        {
            return "Cannot find your word!";
        }
    }

    //Ham sua du lieu tu command line
    public static String editWord(String word_target, String adjusted_explain)
    {
        for (Word i : Dictionary.listWord)
        {
            if (i.getWord_target().equalsIgnoreCase(word_target))
            {
                i.setWord_explain(adjusted_explain);
                return "Adjust Complete!";
            }
        }
        return "Cannot find your word!";
    }

    public static String dictionarySeacher(String s)
    {
        String output = "";
        for (Word word : Dictionary.listWord)
        {
            if (word.getWord_target().length() < s.length())
            {
                continue;
            }
            boolean flag = true;
            char x;
            for (int i = 0; i < s.length(); i++)
            {
                x = word.getWord_target().charAt(i);
                if (s.charAt(i) != x)
                {
                    flag = false;
                    break;
                }
            }
            if (flag)
            {
                output += (word.getWord_target() + '\n');
            }
        }
        if (output.equals("")) return "This word does not exist!";

        return output;
    }
}

