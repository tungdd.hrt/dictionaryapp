import javax.swing.*;
import javax.swing.text.ChangedCharSetException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DictionaryApplication extends JFrame implements ActionListener
{
    private JTextField textField1;
    private JTextArea textArea1;
    private JButton insertButton;
    private JButton changeButton;
    private JButton deleteButton;
    private JButton saveButton;
    private JPanel DictionaryApp;
    private JButton searchButton;
    private JButton translateButton;
    private AdjustMethod adjustMethod;
    private InsertMethod insertMethod;
    private RemoveMethod removeMethod;

    public DictionaryApplication()
    {
        initComponents();
        initActionListener();
        DictionaryAppMethod.insertFromFile();
    }

    private void runAllMethod()
    {
        adjustMethod = new AdjustMethod();
        insertMethod = new InsertMethod();
        removeMethod = new RemoveMethod();
    }

    public void initComponents()
    {
        setTitle("Test App");
        setSize(600, 400);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setContentPane(DictionaryApp);
        setVisible(true);
        runAllMethod();
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource().equals(searchButton))
        {
            setSearchButton();
        }
        if (e.getSource().equals(translateButton))
        {
            setTranslateButton();
        }
        if (e.getSource().equals(insertButton))
        {
            insertMethod.setVisible(true);
        }
        if (e.getSource().equals(changeButton))
        {
            adjustMethod.setVisible(true);
        }
        if (e.getSource().equals(deleteButton))
        {
            removeMethod.setVisible(true);
        }
        if (e.getSource().equals(saveButton))
        {
            JOptionPane.showMessageDialog(this,DictionaryAppMethod.dictionaryExportToFile());
        }
    }

    public void initActionListener()
    {
        insertButton.addActionListener(this);
        changeButton.addActionListener(this);
        deleteButton.addActionListener(this);
        saveButton.addActionListener(this);
        searchButton.addActionListener(this);
        translateButton.addActionListener(this);
    }

    public void setSearchButton()
    {
        String s = textField1.getText();
        String res = DictionaryAppMethod.dictionarySeacher(s);
        textArea1.setText(res);
    }

    public void setTranslateButton()
    {
        String s = textField1.getText();
        String res = DictionaryAppMethod.dictionaryLookup(s);
        textArea1.setText(res);
    }

    public static void main(String[] args)
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                new DictionaryApplication();
            }
        });
    }
}
