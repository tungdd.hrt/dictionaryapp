import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class DictionaryCommandLine
{
    DictionaryManagement dictionaryManagement = new DictionaryManagement();

    /**
     * The method dictionary basic from version 1
     * insert the list Word form Command Line
     * This calls insertFromCommandLine method and showAllWord method of class DictionaryManagement
     */
    public void dictionaryBasic()
    {
        System.out.println("************* Dictionary Basic *************");
        System.out.println("");
        System.out.println("CHOOSE AN OPTION BELOW");
        System.out.println("0: Exit");
        System.out.println("1: Insert a word");
        System.out.println("2: Find a word");
        System.out.println("3: Show word list");
        Scanner reader = new Scanner(System.in);
        char c = reader.next().charAt(0);

        switch (c)
        {
            case '0':
                System.out.println("Exiting");
                return;
            case '1':
                dictionaryManagement.insertFromCommandLine();
                break;
            case '2':
                dictionaryManagement.dictionaryLookup();
                break;
            case '3':
                dictionaryManagement.showAllWord();
            default:
                break;
        }
        dictionaryBasic();
    }

    /**
     * The method dictionary advanced from version 2
     * inset the list word from file
     * This calls inertFromFile method, dictionaryLookup method
     * and showAllWord method of class DictionaryManagement
     *
     * @throws FileNotFoundException
     */
    public void dictionaryAdvanced() throws FileNotFoundException, IOException
    {
        System.out.println("************* Dictionary Advanced *************");
        System.out.println("");
        System.out.println("CHOOSE AN OPTION BELOW");
        System.out.println("0: Exit");
        System.out.println("1: Insert to Dictionary using file");
        System.out.println("2: Show word list");
        System.out.println("3: Find a word");
        System.out.println("4: Insert a word");
        System.out.println("5: Delete a word");
        System.out.println("6: Adjust a word");
        System.out.println("7: Search");

        Scanner reader = new Scanner(System.in);
        char c = reader.next().charAt(0);

        switch (c)
        {
            case '0':
                System.out.println("Exiting");
                return;
            case '1':
                dictionaryManagement.insertFromFile();
                System.out.println("Insert Complete");
                break;
            case '2':
                dictionaryManagement.showAllWord();
                break;
            case '3':
                dictionaryManagement.dictionaryLookup();
                break;
            case '4':
                dictionaryManagement.insertWord();
                break;
            case '5':
                dictionaryManagement.removeWord();
                break;
            case '6':
                dictionaryManagement.editWord();
                break;
            case '7':
                dictionaryManagement.dictionarySeacher();
            default:
                break;
        }
        System.out.println("");
        dictionaryAdvanced();
    }

}