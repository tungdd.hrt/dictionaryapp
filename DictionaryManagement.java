import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;


public class DictionaryManagement
{

    Dictionary myDictionary = new Dictionary();

    /**
     * The method use show all the word in dictionary
     */
    public void showAllWord()
    {
        int count = 1;
        System.out.println("No   | English \t | Vietnamese ");
        for (Word i : Dictionary.listWord)
        {
            System.out.println(count + "   |" + i.getWord_target() + "\t | " + i.getWord_explain());
            count++;
        }
    }

    /**
     * This method use insert list word from command line
     * No parameter and return void
     */

    public void insertFromCommandLine()
    {

        Scanner scan = new Scanner(System.in);
        int numberOfWord;
        System.out.println("Number of words:");
        numberOfWord = Integer.parseInt(scan.nextLine());

        for (int i = 0; i < numberOfWord; i++)
        {
            Word _word = new Word();
            System.out.print(i + 1 + "\nType your word: ");
            _word.setWord_target(scan.nextLine());
            System.out.print("It means: ");
            _word.setWord_explain(scan.nextLine());
            Dictionary.listWord.add(_word);
        }
    }

    /**
     * This method use insert list Word from my file text
     * No parameter and return void
     *
     * @throws FileNotFoundException
     */
    public static boolean empty(final String s)
    {
        return s == null || s.trim().isEmpty();
    }

    public void insertFromFile() throws FileNotFoundException
    {

        try
        {
            File text = new File("dictionary.txt");
            Scanner file = new Scanner(text);
            String a = file.nextLine();
            String target;
            while (file.hasNextLine())
            {
                if (!empty(a))
                {
                    target = a;
                } else
                {
                    target = file.nextLine();
                }
                if (empty(target)) continue;
                String explain = "";
                Word newWord = new Word();

                if (target.charAt(0) == '@')
                {
                    target = target.substring(1);
                    newWord.setWord_target(target);
                    a = file.nextLine();
                    if (empty(a)) continue;

                    while (a.charAt(0) != '@')
                    {
                        explain += a + '\n';
                        if (!file.hasNextLine()) break;
                        a = file.nextLine();
                        if (empty(a)) break;
                    }
                    newWord.setWord_explain(explain);
                }
                if (!empty(newWord.getWord_explain()) && !empty(newWord.getWord_target()))
                {
                    Dictionary.listWord.add(newWord);
                }
            }
        } catch (FileNotFoundException f)
        {
            f.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * find the word in the dictionary and show it's mean
     * return void
     */
    public void dictionaryLookup()
    {
        String a;
        System.out.println("Ban hay nhap tu muon tra:");
        Scanner scan = new Scanner(System.in);
        a = scan.nextLine();
        Dictionary.listWord.stream().filter((i) -> (i.getWord_target().equals(a))).forEachOrdered((i) ->
        {
            System.out.println(a + " means :" + i.getWord_explain());
        });
    }

    /**
     * The method puts all the word
     * of current dictionary in File "DictionaryNow.txt"
     *
     * @throws java.io.FileNotFoundException
     */
    public void dictionaryExportToFile() throws FileNotFoundException, IOException
    {
        FileOutputStream fout = new FileOutputStream("DictionaryNow.txt");
        try (BufferedOutputStream bout = new BufferedOutputStream(fout))
        {
            for (Word i : Dictionary.listWord)
            {
                String line = i.getWord_target() + "\t" + i.getWord_explain();
                bout.write(line.getBytes());
                bout.write(System.lineSeparator().getBytes());

            }
        }
    }

    //Ham them mot tu tu command line
    public void insertWord() throws FileNotFoundException, IOException
    {
        System.out.println("Type your word:");
        Scanner scan = new Scanner(System.in);
        String stringWord = scan.nextLine();
        boolean check = false;
        for (Word i : Dictionary.listWord)
        {
            if (i.getWord_target().equalsIgnoreCase(stringWord))
            {
                System.out.println("This word has already existed!");
                check = true;
                break;
            }
        }
        if (!check)
        {
            System.out.println("It means:");
            String explain = scan.nextLine();
            Dictionary.listWord.add(new Word(stringWord, explain));
            System.out.println("Insert Complete!");
        }

    }

    //Ham xoa mot tu tu command line
    public void removeWord()
    {
        String word_target;
        Scanner scan = new Scanner(System.in);
        System.out.println("Type your word: ");
        word_target = scan.nextLine();
        boolean check = false;
        for (int i = 0; i < Dictionary.listWord.size(); i++)
        {
            if (Dictionary.listWord.get(i).getWord_target().equalsIgnoreCase(word_target))
            {
                Dictionary.listWord.remove(i);
                check = true;
            }
        }
        if (check)
        {
            System.out.println("Removed!");
        } else
        {
            System.out.println("Cannot find your word!");
        }
    }

    //Ham sua du lieu tu command line
    public void editWord()
    {
        System.out.println("Type your word:");
        Scanner scan = new Scanner(System.in);
        String word = scan.nextLine();
        boolean check = false;
        for (Word i : Dictionary.listWord)
        {
            if (i.getWord_target().equalsIgnoreCase(word))
            {
                System.out.println("Change to: ");
                String target = scan.nextLine();
                i.setWord_target(target);
                System.out.println("It means:");
                String explain = scan.nextLine();
                i.setWord_explain(explain);
                check = true;
                break;
            }
        }
        if (!check)
        {
            System.out.println("Cannot find your word!");
        }

    }

    public void dictionarySeacher()
    {
        System.out.println("Type: ");
        String s;
        Scanner scanner = new Scanner(System.in);
        s = scanner.nextLine();
        System.out.println("Results: ");
        Dictionary.listWord.forEach((i) ->
        {
            int index = i.getWord_target().indexOf(s);
            if (index == 0)
            {
                System.out.println(i.getWord_target() + "\t|" + i.getWord_explain());
                return;
            }
        });
    }
}