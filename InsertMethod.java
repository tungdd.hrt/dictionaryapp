import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class InsertMethod extends JFrame implements ActionListener
{
    private JPanel InsertPanel;
    private JTextField textField1;
    private JButton insertButton;
    private JTextArea textArea1;
    private JLabel SearchLabel;
    private JLabel StatusLabel;
    private JLabel Activity;

    public InsertMethod()
    {
        initComponents();
        initActionListener();
    }

    public void initComponents()
    {
        setTitle("Dictionary");
        setSize(600, 400);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setContentPane(InsertPanel);
        setVisible(false);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource().equals(insertButton))
        {
            insertButtonClick();
        }
    }

    public void initActionListener()
    {
        insertButton.addActionListener(this);
    }

    public void insertButtonClick()
    {
        String Target = textField1.getText();
        String explain = textArea1.getText();
        String activity = DictionaryAppMethod.insertWord(Target, explain);
        Activity.setText(activity);
    }
}
