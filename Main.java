import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Main
{
    public static void main(String[] args) throws FileNotFoundException, IOException
    {
        DictionaryCommandLine newCommandLine = new DictionaryCommandLine();
        char c;
        do
        {
            System.out.println("********* Dictionary Command Line **********");
            System.out.println("");
            System.out.println("CHOOSE AN OPTION BELOW");
            System.out.println("0: EXIT");
            System.out.println("1: Simple Commnadline Version");
            System.out.println("2: Advanced Commandline Version");

            Scanner reader = new Scanner(System.in);
            c = reader.next().charAt(0);

            switch (c)
            {
                case '0':
                    System.out.println("Exiting");
                    break;
                case '1':
                    newCommandLine.dictionaryBasic();
                    break;
                case '2':
                    newCommandLine.dictionaryAdvanced();
                    break;
                default:
                    break;
            }
        } while (c != '0');

    }
}