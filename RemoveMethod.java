import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RemoveMethod extends JFrame implements ActionListener
{
    private JTextField textField1;
    private JButton removeButton;
    private JLabel Activity;
    private JPanel RemoveApp;

    public RemoveMethod()
    {
        initComponents();
        initActionListener();
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource().equals(removeButton))
        {
            setRemoveButton();
        }
    }

    public void initComponents()
    {
        setTitle("Test App");
        setSize(600, 100);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setContentPane(RemoveApp);
        setVisible(false);
    }

    public void initActionListener()
    {
        removeButton.addActionListener(this);
    }

    public void setRemoveButton()
    {
        String s = textField1.getText();
        String res = DictionaryAppMethod.removeWord(s);
        Activity.setText(res);
    }
}
